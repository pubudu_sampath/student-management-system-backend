package com.example.managementsystembackend.user;

public enum Role {

    USER,
    ADMIN
}
