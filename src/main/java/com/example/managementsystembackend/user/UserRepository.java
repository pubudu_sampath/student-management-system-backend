package com.example.managementsystembackend.user;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, String> { //repository is created to communicate w the DB


    @Override
    Optional<User> findById(String id);
}
