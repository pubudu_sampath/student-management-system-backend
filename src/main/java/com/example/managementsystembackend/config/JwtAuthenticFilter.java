package com.example.managementsystembackend.config;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

@Component //@Component is an annotation that allows Spring to detect our custom beans automatically.
@RequiredArgsConstructor
public class JwtAuthenticFilter extends OncePerRequestFilter {
    @Override  
    protected void doFilterInternal(
             @NonNull HttpServletRequest request,
             @NonNull HttpServletResponse response,
             @NonNull FilterChain filterChain)
            throws ServletException, IOException {
        final String authHeader= request.getHeader("Authorization");
        final String jwt;

    }
}
