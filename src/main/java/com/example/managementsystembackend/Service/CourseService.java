package com.example.managementsystembackend.Service;

import com.example.managementsystembackend.Entity.Course;
import com.example.managementsystembackend.Repository.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CourseService {

    @Autowired
    private CourseRepository reporitory;

    public Course save(Course course){
        return reporitory.save(course);
    }

    public List<Course> listAll(){
        return reporitory.findAll();
    }

    public  List<Course> searchCourses(String courseSearch){
        List<Course> courses= reporitory.searchCourse(courseSearch);
        return courses;
    }
}
