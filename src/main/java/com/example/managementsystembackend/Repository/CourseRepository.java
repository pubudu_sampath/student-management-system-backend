package com.example.managementsystembackend.Repository;

import com.example.managementsystembackend.Entity.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CourseRepository extends JpaRepository<Course, String> {

    @Query("select c from Course c where "+
    "c.courseId like concat('%',:courseSearch,'%')" +
            "or c.courseName like concat('%',:courseSearch,'%') ")
    List<Course> searchCourse(String courseSearch);
}
