package com.example.managementsystembackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ManagementSystemBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(ManagementSystemBackendApplication.class, args);
	}

}
