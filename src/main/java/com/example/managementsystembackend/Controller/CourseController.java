package com.example.managementsystembackend.Controller;

import com.example.managementsystembackend.Entity.Course;
import com.example.managementsystembackend.Service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@Controller
@RequestMapping("/Course")
public class CourseController {

    @Autowired
    private CourseService service;

    @GetMapping("/getcourses")
    public ResponseEntity<List<Course>> listCourses(){
        List<Course> courses= service.listAll();
        return ResponseEntity.ok(courses);
    }

    @PostMapping("/save")
    public ResponseEntity<Course> addCourse(@RequestBody Course course){
        return ResponseEntity.ok(service.save(course));
    }

    @GetMapping("/search")
    public ResponseEntity<List<Course>> searchCourses(@RequestParam("courseSearch") String courseSearch){
        return ResponseEntity.ok(service.searchCourses(courseSearch));
    }
}
